Description: Basic CRUD using NodeJs, Express, MongoDB
```
$ npm i
```

## Start server

```
$ node server.js
```

## Watch server

Must use nodemon or any package that similar

```
$ nodemon server.js
```